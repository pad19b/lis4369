> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Anaconda, R Studio, and Visual Studio Code
    - Create a1_tip_calculator application and Jupyter Notebook
    - Create Bitbucket class repo and tutorial repo
    - provide git commands and descriptions
    - Provide screenshots

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create and Run Payroll Calculator App
    - Create and Run Sqft to Acres App (SS1)
    - Create and Run Miles per Gallon (SS2)
    - Create and Run Student Percentage App (SS3)
    - Screenshots of Apps running in IDLE
    - Screenshot and Link of A2.ipynb (Payroll Calculator) 
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create and Run Painting Estimator App
    - Create and Run Calorie Percentage (SS4)
    - Create and Run Python Selection Structures (SS5)
    - Create and Run Python Loops (SS6)
    - Screenshots of Apps Running
    - Screenshot and Link of A3.ipynb (Payroll Calculator)

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create and run data_analysis_1 program
    - Includes demo.py
    - Update conda with panda packages
    - Create and run Using Lists App (SS7)
    - Create and run Using Tuples App (SS8)
    - Create and run Using Sets App (SS9)
    - Screenshots of Apps Running
    - Screenshot and Link of P1.ipynb (data_analysis_1)

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create and run data_analysis_2 program
    - Includes demo.py
    - Create and run Using Dictionaries App (SS10)
    - Create and run Random Number App (SS11)
    - Create and run Temperature Conversion App (SS12)
    - Screenshots of Apps Running
    - Screenshot and Link of a4.ipynb (data_analysis_2)
    
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Read through Intro to R Tutorial and create learn_to_use_r.R file
    - Code and run lis4369_a5.R file
    - Create and run Sphere Volume Calculator (SS13)
    - Create and run Calculator with Error Handling (SS14)
    - Create and run File Write/Read (SS15)
    - Include screenshots of both R files running

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create and run lis4369_p2.R file
    - Include link to lis4369_p2_output.txt file
    - Include at least one RStudio screenshot of lis4369_p2.R running
    - Include at least two plots with name in the title
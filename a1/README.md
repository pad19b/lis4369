> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment A1 Requirements:

*Five Parts:*

1. Install Anaconda, R Studio, and Visual Studio Code
2. Create a1_tip_calculator application and Jupyter Notebook
3. Create Bitbucket class repo and tutorial repo
4. provide git commands and descriptions
5. Provide screenshots

#### README.md file should include the following items:

* Screenshot of installs
* Screenshot of a1_tip_calculator running in IDLE and VSC
* Link to A1.jpynb 
* git commands with descriptions
* Link to Bitbucket tutorial repo

#### Assignment Screenshots:

*Screenshot of Anaconda install

![Anaconda install Screenshot](img/Anaconda.png)

*Screenshot of R Studio install:

![R Studio install Screenshot](img/Rstudio.png)

*Screenshot of Visual Studio Code install:

![Visual Studio Code install Screenshot](img/VSC.png)

*Screenshot of a1_tip_calculator (IDLE):

![a1_tip_calculator (IDLE) Screenshot](img/a1_tip_calculator_idle.png)

*Screenshot of a1_tip_calculator (VSC):

![a1_tip_calculator (VSC) Screenshot](img/a1_tip_calculator_vsc.png)

*Screenshot of a1_tip_calculator Jupyter Notebook:

![a1_tip_calculator Jupyter Notebook Screenshot](img/a1_tip_calculator_jupyter.png)

*Link: [a1_tip_calculator.ipynb](a1_tip_calculator.ipynb)

> #### Git commands w/short descriptions:
>
>1. git init - create an empty Git repository or reinitialize an existing one
>2. git status - show the working tree status
>3. git add - add file contents to the index
>4. git commit - record changes to the repository
>5. git push - update remote refs along with associated objects
>6. git pull - fetch from and integrate with another repository or a local branch
>7. git log - show commit logs

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Bitbucket Station Locations Link](https://bitbucket.org/pad19b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

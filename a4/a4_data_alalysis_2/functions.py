# A4 Data Analytics (functions)
# Patrick Duke
# LIS 4369

def get_requirements():
    print("Data Analysis 2")
    print("\nProgram Requirements:\n"
            + "1. Run demo.py.\n"
            + "2. If errors, more than likely misasing installations.\n"
            + "3. Test Python Package Installer.\n"
            + "4. Research how to install any missing packages:\n"
            + "5. Create at least three functions that are called by the program:\n"
            + "     a. main()\n"
            + "     b. get_requirements()\n"
            + "     c. data_analysis()\n"
            + "6. Display graph as per instructions within demo.py")
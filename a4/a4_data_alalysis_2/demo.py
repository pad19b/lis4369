# A4 Data Analytics (demo)
# Patrick Duke
# LIS 4369
# # Pandas = "Python Data Analysis Library"

from numpy import maximum, minimum


def data_analysis():
    import re
    import numpy as np
    np.set_printoptions(threshold=np.inf)
    import pandas as pd
    import matplotlib.pyplot as plt
    
    # Read CSV file into DataFrame

    url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
    df = pd.read_csv(url)

    print("DataFrame composed of three components: index, columns, and data (or values).")
    index = df.index
    columns = df.columns
    values = df.values

    print("\n1. Print indexes:")
    print(index)

    print("\n2. Print columns:")
    print(columns)

    print("\n3. Print columns (a different way):")
    print(df.columns[:])

    print("\n4. Print values, in array format:")
    print(values)

    print("\n5. Print data types:")
    print("\na. index type:")
    print(type(index))

    print("\nb. columns type:")
    print(type(columns))

    print("\nc. values type:")
    print(type(values))

    print("\n6. Print summary of DataFrame:")
    print(df.info())

    print("\n7. First five lines (all columns")
    print(df.head(5))

    df = df.drop('Unnamed: 0', 1)
    print("\n8. Print summary of DataFrame, after dropping column 'Unnamed: 0': ")
    print(df.info())

    print("\n9. First five lines, after dropping column 'Unnamed: 0': ")
    print(df.head(5))

    print("\n Precise data selection using data slicing: ")
    print("\n10. Using iloc, return first three rows: ")
    print(df.iloc[:3])

    print("\n11. Using iloc, return first three rows, starting on index 1310 to end: ")
    print(df.iloc[1310:])

    print("\n12. Select rows 1, 3, and 5; and columns 2, 4, and 6: ")
    a = df.iloc[[0, 2, 4], [1, 3, 5]]
    print(a)

    print("\n13. Select all rows; and columns 2, 4, and 6: ")
    a = df.iloc[:, [1, 3, 5]]
    print(a)

    print("\n14. Select rows 1, 3, and 5; and all columns: ")
    a = df.iloc[[0, 2, 4], :]
    print(a)

    print("\n15. Select all rows, and all columns: ")
    a = df.iloc[:, :]
    print(a)

    print("\n16. Select all rows, and all columns, starting at column 2: ")
    a = df.iloc[:, 1:]
    print(a)

    print("\n17. Select row 1 and column 1: ")
    a = df.iloc[0:1, 0:1]
    print(a)

    print("\n18. Select rows 3-5 and columns 3-5: ")
    a = df.iloc[2:5, 2:5]
    print(a)

    print("\n19. Convert pandas DataFrame df to NumPy ndarray, using values command: ")
    b = df.iloc[:, 1:].values

    print("\n20. Print data frame type: ")
    print(type(df))

    print("\n21. Print a type: ")
    print(type(a))

    print("\n22. Print b type: ")
    print(type(b))

    print("\n23. Print number of dimensions and items in array (rows and columns): ")
    print(b.shape)

    print("\n24. Print items in array: ")
    print(b.dtype)

    print("\n25. Printing a: ")
    print(a)

    print("\n26. Length of a: ")
    print(len(a))

    print("\n27. Printing b: ")
    print(b)

    print("\n28. Length of a: ")
    print(len(b))

    print("\n29. Print element of (NumPy array) ndarray b in 2nd row, 3rd column: ")
    print(b[1, 2])

    print("\n30. Print all records for NumPy array column 2: ")
    print(b[:, 1])

    print("\n31. Get passengers names: ")
    names = df["Name"]
    print(names)

    print("\n32. Find all passengers with name 'Allison': ")
    for name in names:
        print(re.search(r'(Allison)', name))

    print("\n33. Statistical Analysis")
    print("\na. Print mean age: ")
    avg = df["Age"].mean()
    print(avg)

    print("\nb. Print mean age rounded to two decimal places: ")
    avg = df["Age"].mean()
    avg = round(avg, 2)
    print(avg)

    print("\nc. Print mean of every column in DataFrame: ")
    avg_all = df.mean(axis=0)
    print(avg_all)

    print("\nd. Print summary statistics: ")
    describe = df['Age'].describe()
    print(describe)

    print("\ne. Print minimum age: ")
    minimum = df["Age"].min()
    print(minimum)

    print("\nf. Print maximum age: ")
    maximum = df["Age"].max()
    print(maximum)

    print("\ng. Print median age: ")
    median = df["Age"].median()
    print(median)

    print("\nh. Print mode age: ")
    mode =df["Age"].mode()
    print(mode)

    print("\ni. Print number of values: ")
    ct = df["Age"].count()
    print(ct)

    print("\nGraph: Display the first 20 passengers: ")
    c = df['Age'].head(20)
    c.plot()
    plt.xticks(np.arange(0, 20, 1.0))
    plt.show()
# Random Number Generator (functions)
# Patrick Duke
# LIS 4369
import random
from typing import MutableSequence

def get_requirements():
    print("Random Number Generator")
    print("\nProgram Requirements:\n"
            + "1. Get user beginning and ending integer values, and store in two variables.\n"
            + "2. Display 10 random numbers between, and including, above values.\n"
            + "3. Must use integer data types.\n"
            + "4. Example 1: Using range() and randint() functions.\n"
            + "5. Example 2: Using a list with range() and shuffle() functions.\n")

# IPO Input > Process > Output            

def example1(beg, end):
    ran = range(beg, end)
    for x in ran:
        y = (random.randint(beg, end))
        print(y, end=" ")

def example2(beg, end):
    ran = range(beg, end)
    list = []
    for x in ran:
        list.append(x)
    
    random.shuffle(list)

    for x in list:
        print(x, end=" ")

# Python Dictionaries (functions)
# Patrick Duke
# LIS 4369

def get_requirements():
    print("Python Dictionaries")
    print("\nProgram Requirements:\n"
            + "1. Dictionaries (Python data structure): unordere key:value pairs.\n"
            + "2. Dictionary: an associative array (also known as hashes).\n"
            + "3. Any key in a dictionary is associated (or mapped) to a value (i.e. any data type).\n"
            + "4. Keys: must be of immutable type and must be unique.\n"
            + "5. Values: can be any data types and can repeat.\n"
            + "6. Create a program that mirrors the following IPO format.\n"
            + "     create empty dictionary, using curly braces {}: my_dictionary = {}\n"
            + "     use the following keys: fname, lname, degree, major, gpa\n")

# IPO Input > Process > Output            

def using_dictionaries():
    # initialize variables
    v_fname = ""
    v_lname = ""
    v_degree = ""
    v_major = ""
    v_gpa = ""
    my_dictionary = {}

    # Input
    print("Input:")
    v_fname = input("First Name: ")
    v_lname = input("Last Name: ")
    v_degree = input("Degree: ")
    v_major = input("Major: ")
    v_gpa = input("GPA: ")

    print()

    # Process
    my_dictionary['fname'] = v_fname
    my_dictionary['lname'] = v_lname
    my_dictionary['degree'] = v_degree
    my_dictionary['major'] = v_major
    my_dictionary['gpa'] = v_gpa

    #Output
    print("\nOutput")
    print("Print my_dictionary: ")
    print(my_dictionary)

    print("\nReturn view of dictionary's key value pair: ")
    print(my_dictionary.items())

    print("\nReturn view object of all keys: ")
    print(my_dictionary.keys())

    print("\nReturn view object of all values in dictionary: ")
    print(my_dictionary.values())

    print("\nPrint only the first and last names: ")
    print(my_dictionary['fname'], my_dictionary['lname'])

    print("\nPrint only first and last names using get() function: ")
    print(my_dictionary.get("fname"), my_dictionary.get("lname"))

    print("\nCount number of items in dictionary: ")
    print(len(my_dictionary))

    print("\nRemove last dictionary item: ")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary, using key: ")
    my_dictionary.pop("major")
    print(my_dictionary)

    print("\nReturn object type: ")
    print(type(my_dictionary))

    print("\nDelete all items from list: ")
    my_dictionary.clear()
    print(my_dictionary)

    del my_dictionary
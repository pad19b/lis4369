# Temperature Conversion(functions)
# Patrick Duke
# LIS 4369

from numpy.core.fromnumeric import choose


def get_requirements():
    print("Temperature Conversion")
    print("\nProgram Requirements:\n"
            + "1. Program converts user-entered temperatures into Fahrenheit or Celsius.\n"
            + "2. Program continues to prompt for user entry until no longer requested.\n"
            + "3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted\n"
            + "4. Note: Program does not validate numeric data.\n")

# IPO Input > Process > Output            

def temperature_conversion():
    temp = 0.0
    choice = ' '
    type = ' '

    print("Input:")

    choice = input("Do you want to convert a temperature (y or n)? ").lower()

    print("\nOutput:")

    while (choice[0] == 'y'):
        type = input("Fahrenheit to Celsius? Type: f\n"
                        + "Celsius to Fahrenheit? Type: c\n"
                        + "Enter: ").lower()

        if type[0] == 'f':
            temp = float(input("Enter temperature in Fahrenheit: "))
            temp = ((temp - 32)*5)/9
            temp = round(temp, 2)
            print("Temperature in Celsius = " + str(temp))
            choice = input("Do you want to convert another temperature (y or n)? ").lower()

        elif type[0] == 'c':
            temp = float(input("Enter temperature in Celsius: "))
            temp = (temp * 9/5) + 32
            temp = round(temp, 2)
            print("Temperature in Fahrenheit = " + str(temp))
            choice = input("Do you want to convert another temperature (y or n)? ").lower()

        else:
            print("Incorrect entry. Please try again.")
            choice = input("\nDo you want to convert a temperature (y or n)? ").lower()

    print("\nThank your for using our Temperature Conversion Program!")
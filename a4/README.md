> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment A4 Requirements:

*Seven Parts:*

1. Create and run data_analysis_2 program
2. Includes demo.py
3. Create and run Using Dictionaries App (SS10)
4. Create and run Random Number App (SS11)
5. Create and run Temperature Conversion App (SS12)
6. Screenshots of Apps Running
7. Screenshot and Link of a4.ipynb (data_analysis_2)

#### README.md file should include the following items:

* Screenshot of all apps running
* Screenshot and Link of a4.ipynb
* Screenshot of demo Graph

#### Assignment Screenshots:

*Screenshot of a4.ipynb*:

![a4.ipynb Screenshot](img/jupyter1.png)

![a4.ipynb Screenshot](img/jupyter2.png)

![a4.ipynb Screenshot](img/jupyter3.png)

![a4.ipynb Screenshot](img/jupyter4.png)

![a4.ipynb Screenshot](img/jupyter5.png)

![a4.ipynb Screenshot](img/jupyter6.png)

![a4.ipynb Screenshot](img/jupyter7.png)

![a4.ipynb Screenshot](img/jupyter8.png)

![a4.ipynb Screenshot](img/jupyter9.png)

![a4.ipynb Screenshot](img/jupyter10.png)

![a4.ipynb Screenshot](img/jupyter11.png)

![a4.ipynb Screenshot](img/jupyter12.png)

![a4.ipynb Screenshot](img/jupyter13.png)

*Link: [a4.ipynb](a4_data_analysis_2/a4.ipynb)

*Screenshot of Demo Graph*:

![Demo Graph Screenshoot](img/graph.png)

*Screenshot of Using Dictionaries App *:

![Using Dictionaries App Screenshoot](img/dictionary.png)

*Screenshot of Random Number App*:

![Random Number App Screenshoot](img/random_numbers.png)

*Screenshot of Temperature Conversion App*:

![Temperature Conversion App Screenshoot](img/temp.png)

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment P2 Requirements:

*Four Parts:*

1. Create and run lis4369_p2.R file
2. Include link to lis4369_p2_output.txt file
3. Include at least one RStudio screenshot of lis4369_p2.R running
4. Include at least two plots with name in the title

#### README.md file should include the following items:

* Screenshot of RStudio running
* Screenshot of two plots
* link to lis4369_p2_output.txt
* link to pdf file of graphs 

#### Assignment Screenshots:

*Screenshot of RStudio running*

![RStudio Screenshot](img/Rstudio.png)

*Screenshot of 1st plot*

![1st Plot Screenshot](img/Graph1.png)

*Screenshot of 2nd plot*

![2nd Plot Screenshot](img/Graph2.png)

#### Assignment Links:

Link: [lis4369_p2_output.txt](lis4369_p2_output.txt)

Link: [lis4369_p2_plots.pdf](lis4369_p2_plots.pdf)
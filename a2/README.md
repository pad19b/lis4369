> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment A1 Requirements:

*Six Parts:*

1. Create and Run Payroll Calculator App
2. Create and Run Sqft to Acres App (SS1)
3. Create and Run Miles per Gallon (SS2)
4. Create and Run Student Percentage App (SS3)
5. Screenshots of Apps running in IDLE
6. Screenshot and Link of A2.ipynb (Payroll Calculator)

#### README.md file should include the following items:

* Screenshot of all Apps running in IDLE
* Screenshot A2.ipynb (Payroll Calculator)

#### Assignment Screenshots:

*Screenshot of Payroll Calculator App*:

![Payroll Calculator Screenshoot](img/payroll_calculator.png)

*Screenshot of A2.ipynb*:

![A2.ipynb Screenshot](img/jupyter1.png)

![A2.ipynb Screenshot](img/jupyter2.png)

*Link: [a2.ipynb](A2.ipynb)

*Screenshot of Sqft to Acres App*:

![Sqft to Acres App Screenshoot](img/sqfy_to_acres.png)

*Screenshot of Miles per Gallon*:

![Miles per Gallon Screenshoot](img/miles_per_gallon.png)

*Screenshot of Student Percentage App*:

![Student Percentage Screenshoot](img/student_percentage.png)

# Student Percentage (functions)
# Patrick Duke
# LIS 4369

def print_requirements():
    print("IT/ICT Student Percentage\n")
    print("Developer: Patrick Duke\n")
    print("Program Requirements: \n"
            + "1. Find the number of IT/ICT students in class. \n"
            + "2. Calculate IT/ICT student percentage \n"
            + "3. Must use float data type. \n"
            + "3. Format, right-align numbers, and round to two decimal places \n")

def calculate_percent():
    # IPO: Input > Process > Output
    
    # Input
    print("Input:")
    it_std = float(input('Enter number of IT students: '))
    ict_std = float(input('Enter number of ICT students: '))

    # Process
    tot_std = it_std + ict_std
    it_percent = (it_std / tot_std) * 100
    ict_percent = (ict_std / tot_std) * 100

    # Output
    print("\nOutput: ")
    print(f'Total Students: {tot_std:>8.2f}')
    print(f'IT Students:  {it_percent:10.2f}%')
    print(f'ICT Students:  {ict_percent:9.2f}%\n')


# Square Feet to Acres (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.print_requirements()
    f.calculate_sqft_to_acre()

if __name__ == "__main__":
    main()

# Square Feet to Acres (functions)
# Patrick Duke
# LIS 4369

def print_requirements():
    print("IT/ICT Student Percentage\n")
    print("Developer: Patrick Duke\n")
    print("Program Requirements: \n"
            + "1. Research the number of square feet to an acre. \n"
            + "2. Must use float data type for user input and calculations. \n"
            + "3. Format and round conversion to two decimal places. \n")

def calculate_sqft_to_acre():
    # IPO: Input > Process > Output
    
    # number of square feet in an acre (constant)
    SQ_FEET_PER_ACRE = 43560

    # Input
    print("Input:")
    tract_size = 0.0
    acres = 0.0

    tract_size = float(input('Enter number of square feet: '))
    

    # Process
    acres = tract_size / SQ_FEET_PER_ACRE

    # Output
    print("\nOutput: ")
    print("{0:,.2f} {1} {2:,.2f} {3}".format(tract_size, "square feet =", acres, "acres"))
    


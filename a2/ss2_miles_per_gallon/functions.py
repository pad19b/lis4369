# Miles Per Gallon (functions)
# Patrick Duke
# LIS 4369

def print_requirements():
    print("Miles Per Gallon\n")
    print("Developer: Patrick Duke\n")
    print("Program Requirements: \n"
            + "1. Calculate MPG \n"
            + "2. Must use float data type for user input and calculations. \n"
            + "3. Format and round conversion to two decimal places \n")

def calculate_mpg():
    # IPO: Input > Process > Output
    
    # Input
    print("Input:")
    miles = float(input('Enter miles driven: '))
    gals = float(input('Enter gallons of fuel used: '))

    # Process
    mpg = miles / gals

    # Output
    print("\nOutput: ")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f} {5}".format(miles, "miles driven and", gals, "gallons used = ", mpg, "miles per gallon"))

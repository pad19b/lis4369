# Miles Per Gallon (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.print_requirements()
    f.calculate_mpg()

if __name__ == "__main__":
    main()

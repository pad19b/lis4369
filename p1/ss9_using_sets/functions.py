# Using Sets (functions)
# Patrick Duke
# LIS 4369

def get_requirements():
    print("Using Sets")
    print("\nProgram Requirements:\n"
            + "1. Sets (Python data structure): mmutable, heterogeneous, unordered sequence of elements.\n"
            + "2. Sets can insert, update, and delete.\n"
            + "3. While Sets are mutable, they cannot contain other mutable items like a list.\n"
            + "4. Sets are unordered and thus, do not use indexing or slicing.\n"
            + "5. Two methods to create sets:\n"
            + "     a. Create using curly brackets {set}: my_set = {1, 3.14, 2.0, 'four', 'Five'}\n"
            + "     b. Create using set() function: my_set = set(<iterable>)\n"
            + "6. Create a program using the IPO format (input/process/output).\n")

# IPO Input > Process > Output            

def using_sets():
    # input
    print("\nInput: Hard coded - no user input.")
    
    # variables and set
    my_set = {1, 3.14, 2.0, 'four', 'Five'}
    print("Print my_set created using curly brackets:")
    print(my_set)

    # output
    print("\nPrint type of my_set:")
    print(type(my_set))

    my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
    print("\nPrint my_set1 created using set() function with list:")
    print(my_set1)

    print("\nPrint type of my_set1:")
    print(type(my_set1))

    my_set2 = set((1, 3.14, 2.0, 'fout', 'Five'))
    print("\nPrint my_set2 created using set() function with tuple:")
    print(my_set2)

    print("\nPrint type of my_set2:")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDiscard 'four':")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five':")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nAdd element to set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDisplay minimum number:")
    print(min(my_set))

    print("\nDisplay maximum number:")
    print(max(my_set))

    print("\nDisplay sum of numbers:")
    print(sum(my_set))

    print("\nDelete all set elements:")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))
# Using Tuples (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.get_requirements()
    f.using_tuples()
    
if __name__ == "__main__":
    main()

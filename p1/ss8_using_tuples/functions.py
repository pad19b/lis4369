# Using Tuples (functions)
# Patrick Duke
# LIS 4369

def get_requirements():
    print("Using Tuples")
    print("\nProgram Requirements:\n"
            + "1. Tuples (Python data structure): *immutable* (cannot be changed), ordered sequence of elements.\n"
            + "2. Tuples are immutable/unchangeable - that is, cannot insert, update, delete (individual items).\n"
            + "3. Create tuple using parentheses (tuple) = ('cherries', 'apples', 'bannans', 'oranges').\n"
            + "4. Create tuple (packing), that is, without using parenteses.\n"
            + "5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables.\n"
            + "6. Create a program that mirrors the following IPO (inpot/process/output) format.\n")

# IPO Input > Process > Output            

def using_tuples():
    # input
    print("\nInput: Hard coded - no user input.")
    
    # variables and tuple
    my_tuple1 = ("cherries", "apples", "bananas", "oranges")

    my_tuple2 = 1, 2, "three", "four"

    # output
    print("\nOutput:")
    print("Print my_tuple1:")
    print(my_tuple1)

    print()

    print("Print my_tuple2:")
    print(my_tuple2)

    print()

    fruit1, fruit2, fruit3, fruit4 = my_tuple1
    print("Print my_tuple unpacking:")
    print(fruit1, fruit2, fruit3, fruit4)

    print()

    print("Print the third element in my_tuple2:")
    print(my_tuple2[2])

    print()

    print("Print second and third elements: ")
    print(my_tuple1[1:3])

    print()

    print("Reassign my_tuple2 using parentheses.")
    my_tuple2 = (1, 2, 3, 4)
    print("Print my_tuple2: ")
    print(my_tuple2)

    print()

    print("Reassign my_tuple2 using packing method (no parentheses).")
    my_tuple2 = 5, 6, 7, 8
    print("Print my_tuple2: ")
    print(my_tuple2)

    print()

    print("Print number of elements in my_tuple1: " + str(len(my_tuple1)))

    print()

    print("Print type of my_tuple1: " + str(type(my_tuple1)))

    print()

    print("Delete my_tuple1:")
    del my_tuple1
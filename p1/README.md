> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment P1 Requirements:

*Eight Parts:*

1. Create and run data_analysis_1 program
2. Includes demo.py
3. Update conda with panda packages
4. Create and run Using Lists App (SS7)
5. Create and run Using Tuples App (SS8)
6. Create and run Using Sets App (SS9)
7. Screenshots of Apps Running
8. Screenshot and Link of P1.ipynb (data_analysis_1)

#### README.md file should include the following items:

* Screenshot of all apps running
* Screenshot and Link of p1.ipynb
* Screenshot of demo Graph

#### Assignment Screenshots:

*Screenshot of data_analysis_1 program*:

![data_analysis_1 program Screenshoot](img/demo_running.png)

*Screenshot of p1.ipynb*:

![p1.ipynb Screenshot](img/jupyter1.png)

![p1.ipynb Screenshot](img/jupyter2.png)

*Link: [p1.ipynb](p1.ipynb)

*Screenshot of Demo Graph*:

![Demo Graph App Screenshoot](img/graph.png)

*Screenshot of Using Lists*:

![Using Lists App Screenshoot](img/using_lists.png)

*Screenshot of Using Tuples*:

![Using Tuples Screenshoot](img/using_tuples.png)

*Screenshot of Using Sets*:

![Using Sets Screenshoot](img/using_sets.png)

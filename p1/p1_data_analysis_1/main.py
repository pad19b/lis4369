# P1 Data Analytics (main)
# Patrick Duke
# LIS 4369

import functions as f
import demo as d

def main():
    f.get_requirements()
    d.data_analysis()
    
if __name__ == "__main__":
    main()

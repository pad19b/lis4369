# Calculator with Error Handling (functions)
# Patrick Duke
# LIS 4369

import math as m

def get_requirements():
    print("Using Lists")
    print("\nProgram Requirements:\n"
            + "1. Program calculates two numbers and rounds to two decimal places.\n"
            + "2. Prompt user for two numbers, and a sutiable operator.\n"
            + "3. Use Pythno error handling to validate data.\n"
            + "4. Test and correct arithmetic operator.\n"
            + "5. Division by zero not permitted.\n"
            + "6. Program loops until correct input entered - numbers and arithmetic operator.\n"
            + "7. Replicate display below.\n")

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a number! Try again: ")
        
def getOP():
    validOperators = ['+','-','*','/','//','%','**']
    while True:
        op = input("\nSutiable Operators: \n+ \n- \n* \n/ \n// (integer division) \n% (modulo operator) \n** (power) \nEnter: ")
        try:
            validOperators.index(op)
            return op
        except ValueError:
            print("Invalid operator! Try again: ")

def calc():
    num1 = getNum("Enter first number: ")
    num2 = getNum("Enter second number: ")
    op = getOP()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break
            except ZeroDivisionError:
                num2 = getNum("You can't divide by zero! Re-enter second number: ")
    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("You can't divide by zero! Re-enter second number: ")
    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("You can't divide by zero! Re-enter second number: ")
    
    print("\nAnswer is " + str(round(sum,2)))
    print()
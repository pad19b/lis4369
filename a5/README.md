> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment A5 Requirements:

*Six Parts:*

1. Read through Intro to R Tutorial and create learn_to_use_r.R file
2. Code and run lis4369_a5.R file
3. Create and run Sphere Volume Calculator (SS13)
4. Create and run Calculator with Error Handling (SS14)
5. Create and run File Write/Read (SS15)
6. Include screenshots of both R files running

#### README.md file should include the following items:

* Screenshot of four plots of the tutorial file
* Screenshot of two plots of the assignment file
* Screenshot of the Skill Sets

#### Assignment Screenshots:

*Screenshot learn_to_use_r.R plots*:

![tutorial Screenshoot](img/tutorial1.png)

![tutorial Screenshoot](img/tutorial2.png)

![tutorial Screenshoot](img/tutorial3.png)

![tutorial Screenshoot](img/tutorial4.png)

*Screenshot of lis4369_a5.R plots*:

![assignment file Screenshoot](img/a5.1.png)

![assignment file Screenshoot](img/a5.2.png)

![assignment file Screenshoot](img/a5.3.png)

*Screenshot of Sphere Volume Calculator*:

![Sphere Volume Calculators App Screenshoot](img/sphere_volume_calculator.png)

*Screenshot of Calculator with Error Handling*:

![Calculator with Error Handling Screenshoot](img/calculator_with_error_handling.png)

*Screenshot of File Write/Read*:

![File Write/Read Screenshoot](img/file_write_read.png)

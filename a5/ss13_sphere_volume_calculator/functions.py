# Sphere Volume Calculator (functions)
# Patrick Duke
# LIS 4369

from typing import MutableMapping
import math as m

def get_requirements():
    print("Sphere Volume Calculator")
    print("\nProgram Requirements:\n"
            + "1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches and rounds to two decimal places.\n"
            + "2. Must use Python's built in PI and pow() capabilities.\n"
            + "3. Program checks for non-integers and non-numeric values.\n"
            + "4. Program continues to prompt user entry until no longer requested, prompt accepts upper and lower case letters.\n")           

def calc():
    pi = m.pi
    di = 0.0
    frac = 1.3333
    gal = 19.25317
    cho = ' '
    ans = 0

    print("Input: ")
    cho = input("Do you want to calculate the volume of a sphere (y or n)?  ").lower()

    print("Output: ")

    while(cho == 'y'):
        while True:
            try:
                di = int(input("Please enter diameter in inches (integers only): "))
                ra = di * .5

                ans = ((m.pow(ra, 3)) * frac * pi / 12)/ gal
                ans = str(round(ans, 2))
                print("Sphere volume: " + ans + " liquid U.S. gallons.")

                cho = input("Do you want to calculate a sphere volume (y or n)?  ")
            
            except ValueError:
                print("Not an integer! Try again: ")
                continue

            else:
                break
        else:
            print("Thank you for using our Calculator!")
            
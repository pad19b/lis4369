# File Write/Read (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.get_requirements()
    f.write_read_file()
    

if __name__ == "__main__":
    main()

# Python Selection Structures (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.print_requirements()
    f.get_input()
    f.operate()

if __name__ == "__main__":
    main()

# Python Selection Structures (functions)
# Patrick Duke
# LIS 4369

def print_requirements():
    print("Python Selection Structures\n")
    print("Program Requirements: \n"
            + "1. Use Python selection structure. \n"
            + "2. Prompt user for two numbers and a sutiable operator. \n"
            + "3. Test for correct numeric operator. \n"
            + "4. Replicate display below. \n")

def get_input():
    # get number and operator from user
    print("Python Calculator")
    num1 = input("Enter 1st number: ")
    num2 = input("Enter 2nd number: ")
    print("\nSutible Operators: \n + \n - \n * \n / \n // (integer division) \n % (modulo operator) \n ** (power) \n")
    operator = input("Enter operator: ")

    # check for valid input
    if num1.isnumeric() == True and num2.isnumeric() == True:
        n1 = float(num1)
        n2 = float(num2)
        operate(n1, n2, operator)
    else:
        print("Error: Please enter valid numbers")
    
def operate(num1, num2, operator):
    # check to see if operator is valid
    if operator == "+":
        num3 = num1 + num2
        print(num3)
    elif operator == "-":
        num3 = num1 - num2
        print(num3)
    elif operator == "*":
        num3 = num1 * num2
        print(num3)
    elif operator == "/":
        if (num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 / num2
            print(num3)
    elif operator == "//":
        if (num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 // num2
            print(num3)
    elif operator == "%":
        if (num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 % num2
            print(num3)        
    elif operator == "**":
        num3 = num1 ** num2
        print(num3)   
    else:
        print("Error: Illegal operator entered")         

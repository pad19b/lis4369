# Painting Estimator (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.print_requirements()
    f.estimate_painting_cost()

if __name__ == "__main__":
    main()

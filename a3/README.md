> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Patrick Duke

### Assignment A1 Requirements:

*Six Parts:*

1. Create and Run Painting Estimator App
2. Create and Run Calorie Percentage (SS4)
3. Create and Run Python Selection Structures (SS5)
4. Create and Run Python Loops (SS6)
5. Screenshots of Apps Running
6. Screenshot and Link of A3.ipynb (Payroll Calculator)

#### README.md file should include the following items:

* Screenshot of all Apps running
* Screenshot A2.ipynb (Painting Estimator)

#### Assignment Screenshots:

*Screenshot of Painting Estimator App*:

![Painting Estimator Screenshoot](img/painting_estimator.png)

*Screenshot of A3.ipynb*:

![A3.ipynb Screenshot](img/jupyter1.png)

![A3.ipynb Screenshot](img/jupyter2.png)

*Link: [A3.ipynb](A3.ipynb)

*Screenshot of Calorie Percentage*:

![Calorie Percentage App Screenshoot](img/ss4_calorie_percent.png)

*Screenshot of Python Structures*:

![Python Structures Screenshoot](img/ss5_python_structures1.png)

![Python Structures Screenshoot](img/ss5_python_structures2.png)

*Screenshot of Python Loops*:

![Python Loops Screenshoot](img/ss6_python_loops1.png)

![Python Loops Screenshoot](img/ss6_python_loops2.png)

# Python Loops (main)
# Patrick Duke
# LIS 4369

import functions as f

def main():
    f.print_requirements()
    f.do_loops()
    
if __name__ == "__main__":
    main()

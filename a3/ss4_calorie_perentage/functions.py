# Calorie Percentage (functions)
# Patrick Duke
# LIS 4369

def get_requirements():
    print("Calorie Percentage")

def calculate_percent():
    FAT_CAL = 9
    CARB_CAL = 4
    PROTEIN_CAL = 4

    print("Input:")
    fat_gram = float(input("Enter total fat grams: "))
    carb_gram = float(input("Enter total carb grams: "))
    protein_gram = float(input("Enter total protein grams: "))

    # 1 gram of fat = 9 cal
    total_fat = fat_gram * FAT_CAL

    # 1 gram of carb = 4 cal
    total_carb = carb_gram * CARB_CAL

    # 1 gram of protein = 4 cal
    total_protein = protein_gram * PROTEIN_CAL

    #total calories
    total_cal = total_fat + total_carb + total_protein

    # calculate percentage
    fat_percent = (total_fat / total_cal) * 100
    carb_percent = (total_carb / total_cal) * 100
    protein_percent = (total_protein / total_cal) * 100

    print("\nOutput: ")
    print("{0:<12} {1:<12} {2:<12}".format("Type", "Calories", "Percentage"))
    print("{0:<12} {1:<12,.2f} {2:<8.2f} {3}".format("Fat", total_fat, fat_percent, "%"))
    print("{0:<12} {1:<12,.2f} {2:<8.2f} {3}".format("Carbs", total_carb, carb_percent, "%"))
    print("{0:<12} {1:<12,.2f} {2:<8.2f} {3}".format("Protein", total_protein, protein_percent, "%"))